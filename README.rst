=========
BaixAcast
=========

BaixAcast (originally “The Magnus Archives Archiver”) is a simple Python
script to download podcasts episodes available via an Acast RSS feed for
archiving purposes.

Licence
=======

BaixAcast is libre software and licensed under the GPL (GNU General Public
licence), either version 3 or (at your option) any later version. See the
bundled LICENCE file for details.
