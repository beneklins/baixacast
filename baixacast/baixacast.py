#!/usr/bin/env python3

# TODO Add --tag option to pre-prend text to output files.
# TODO Use tqdm.
# TODO Create functions to extract date, title and show notes information.

import os
import argparse
from json import dump as json_dump
from mutagen.id3 import ID3, COMM, APIC, _util
from mutagen.mp3 import EasyMP3
import requests
from datetime import datetime
import dateutil.parser
from bs4 import BeautifulSoup as bs
import urllib
import re

parser = argparse.ArgumentParser(
    description="BaixAcast: Archive Acast podcasts",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-f",
    "--feed",
    help="URL to an Acast RSS feed. Example: http://rss.acast.com/themagnusarchives",
    dest="feed_url",
)
parser.add_argument(
    "-o",
    "--output-dir",
    help="Output directory for downloaded files.",
    dest="output_dir",
)
parser.add_argument(
    "-s",
    "--sample",
    help="Parse only the first 4 items as a test. Default: False",
    action="store_true",
    dest="sample",
    default=False,
)
args = parser.parse_args()

TAG_COVER_URL = "itunes:image"
TAG_DATE = "pubDate"
TAG_EPISODE = "itunes:episode"
TAG_FILE_URL = "enclosure"
TAG_SEASON = "itunes:season"
TAG_SUBTITLE = "itunes:subtitle"
TAG_SUMMARY = "itunes:summary"
TAG_TITLE = "title"

output_dir = args.output_dir
os.makedirs(output_dir, 0o700)


def isodate(s):
    return datetime.strftime(dateutil.parser.parse(s), "%Y-%m-%d")


def output_filename(filename):
    return f"{output_dir}/{filename}"


def get_info(s, want, tag=""):
    if s:
        if tag:
            result = s[0][tag].strip()
        else:
            result = s[0].strip()
    else:
        result = ""
    return result


def get_episode_number(ep):
    episode_number_info = ep.find_all(TAG_EPISODE)
    if episode_number_info:
        episode_number = episode_number_info[0].text.strip()
    else:
        episode_number = ""
    return episode_number


def get_subtitle(ep):
    subtitle_info = ep.find_all(TAG_SUBTITLE)
    if subtitle_info:
        subtitle = subtitle_info[0].text.strip()
    else:
        subtitle = ""
    return subtitle


def get_season_info(ep):
    season_info = ep.find_all(TAG_SEASON)
    if season_info:
        season = "Season " + season_info[0].text.strip()
    else:
        season = "Extra"
    return season


# TODO Move this to an external file.
def parse_title(s):
    s = re.sub(r"^M[Aa][Gg] ", "", s)
    s = re.sub(r"^- ", "", s.strip())
    s = re.sub(r"^(\d+.?[\d+]?) -?", r"MAG \1 ", s)
    s = re.sub(r"\s+", r" ", s)
    return s


def download_file(url, dest=None):
    response = urllib.request.urlopen(url)
    data = response.read()

    if dest:
        # Write data to file.
        file_ = open(dest, "b+w")
        file_.write(data)
        file_.close()
    return data


def add_comment(file, comment):
    mp3_file = ID3(file)
    mp3_file.add(COMM(encoding=3, lang="eng", text=comment))
    mp3_file.save()


def add_album_art(file_name, album_art, image_type="jpg"):
    """
    Add album_art in .mp3's tags
    https://github.com/kalbhor/MusicTools
    """

    # Gets album art from URL.
    img = requests.get(album_art, stream=True)
    img = img.raw

    audio = EasyMP3(file_name, ID3=ID3)

    try:
        audio.add_tags()
    except _util.error:
        pass

    audio.tags.add(
        APIC(
            # UTF-8
            encoding=3,
            mime=f"image/{image_type}",
            # 3 is for album art
            type=3,
            desc="cover",
            # Reads and adds album art.
            data=img.read(),
        )
    )
    audio.save()

    return album_art


feed_url = args.feed_url
print(f"Downloading feed {feed_url}")
feed_data = download_file(feed_url)
bs_content = bs(feed_data, "lxml-xml")
complete_data = []
episodes = bs_content.find_all("item")

all_data = dict()
# Iterate over the episodes in chronological order.
for idx, ep in enumerate(episodes[::-1]):
    data = dict(
        date=isodate(ep.find(TAG_DATE).text.strip()),
        title=parse_title(ep.find(TAG_TITLE).text.strip()),
        subtitle=get_subtitle(ep),
        shownotes=ep.find(TAG_SUMMARY).text,
        season=get_season_info(ep),
        episode=get_episode_number(ep),
    )
    file_url = get_info(ep.find_all(TAG_FILE_URL), "url", tag="url")
    cover_url = get_info(ep.find_all(TAG_COVER_URL), "cover", tag="href")

    filename = output_filename(
        f"{data['date']} {data['season']} - {data['title']}"
    )
    filename_mp3 = filename + ".mp3"
    filename_json = filename + ".json"

    data["filename"] = filename_mp3

    all_data[filename] = data

    print(f"Downloading {filename}")
    download_file(file_url, filename_mp3)
    print(f"\tDownloaded to {filename_mp3}")
    add_comment(filename_mp3, data["shownotes"])
    print(f"\tAdded shownotes")
    if cover_url:
        add_album_art(
            filename_mp3, cover_url, image_type=cover_url.split(".")[-1]
        )
        print(f"\tAdded cover art")
    print("---")
    # Create NFO/RST file with all information?

    if args.sample:
        if idx >= 3:
            break

output_json_file = output_filename("all_episodes_info.json")
with open(output_json_file, "w") as output_file:
    json_dump(all_data, output_file)
print(f"JSON data saved to {output_json_file}")
